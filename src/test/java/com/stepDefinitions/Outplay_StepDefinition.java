package com.stepDefinitions;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;

import com.pageClasses.CommonMethods;
import com.pageClasses.LoginPageClass;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Outplay_StepDefinition  {
	CommonMethods cmnmthds = new CommonMethods ();
	WebDriver driver = CommonMethods.getBrowser();
	/*public Outplay_StepDefinition (CommonMethods commonMethods) throws IOException {
		driver = commonMethods.getdriver(null);
		loginObj = new LoginPageClass(driver);
	}*/
	LoginPageClass loginObj = new LoginPageClass(driver);
	
	

	@Given("user is on outplay {string} login page in {string}")
	public void user_is_on_outplay_login_page_in(String browserName, String environment) throws IOException {
		//driver = cmnmthds.getBrowser();
		
		loginObj.navigateURL(environment);

	}

	@When("user enters valid {string},{string} and clicks login")
	public void user_enters_valid_and_clicks_login(String username, String password) throws IOException, InterruptedException {

		loginObj.credentials(username, password);

	}

	@Then("Dashboard is shown on the welcome page")
	public void Dashboard_is_shown_on_the_welcome_page() {

		loginObj.elmtgetText(loginObj.msgDashboard, 5000, "Dashboard");

	}

	@When("user enters invalid {string},{string} and clicks login")
	public void user_enters_invalid_and_clicks_login(String username, String password) throws IOException, InterruptedException {

		loginObj.invcredentials(username, password);

	}

	@Then("Error message is shown")
	public void error_message_is_shown() {

		loginObj.elmtgetText(loginObj.alrtinvlogin, 5000, "Invaild Email or Password");

	}
	
	@Given("user is on welcome page")
	public void user_is_on_welcome_page() {
		loginObj = new LoginPageClass(driver);
		loginObj.elmtgetText(loginObj.msgDashboard, 5000, "Dashboard");
	}
	
	@When("user clicks Prospect")
	public void user_clicks_Prospect() {
		System.out.println("click prospect");
	}
	
	@Then("Add prospect is shown")
	public void add_prospect_is_shown() {
		System.out.println("Add prospect is shown");
	}
	
@AfterSuite
	public void closeAllDriver() {

		driver.close();
		driver.quit();
	}











}
