package com.runnerClasses;

import io.cucumber.java.Scenario;
import io.cucumber.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;



//@RunWith(Cucumber.class)
@CucumberOptions ( 
				features = {"src/test/resources/featureFiles"},
				tags = "", 
				glue = {"com.stepDefinitions"},
				plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
				dryRun = false, 
				monochrome = true,
				strict = true)

public class Outplay_RunnerClass extends AbstractTestNGCucumberTests {
	
	
@BeforeClass
	public void test () {
		System.out.println("This test executes before each scenario");
	}
		
}
