package com.pageClasses;

import static org.testng.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;


public class CommonMethods {

	public static WebDriver driver;
	public static WebDriverWait wait;

	public static String userDir = System.getProperty("user.dir");
	public static Properties prop = new Properties();
	
	public static Scenario myScenario;


	public static Properties getPropFile() throws IOException {

		String filePath = userDir + "\\src\\test\\resources\\propertiesFiles\\project.properties";
		FileInputStream fis = new FileInputStream (filePath);
		prop.load(fis);
		return prop;
	}

	@Before("@login")
	public  static WebDriver getBrowser() {
		
		try {
			getPropFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String browserPath = userDir + "\\src\\test\\resources\\browserDrivers\\chromedriver.exe";
		String browserName = prop.getProperty("browser");

		switch (browserName) {
		case "Firefox":
			driver = new FirefoxDriver();
			driver.manage.windows.maximize();
			break;
		case "IE":
			System.setProperty("webdriver.ie.driver","C:\\Users\\abc\\Desktop\\Server\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.manage.windows.maximize();
			break;
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", browserPath);
			driver = new ChromeDriver();
			driver.manage.windows.maximize();
			break;
		default:
			System.out.println("Drivers not initiated");

		}

		return driver;

	}
	
	

	public WebDriver getdriver(String browser) throws IOException {
		String browserPath = userDir + "\\src\\test\\resources\\browserDrivers\\chromedriver.exe";
		String filePath = userDir + "\\src\\test\\resources\\propertiesFiles\\project.properties";
		FileInputStream fis = new FileInputStream (filePath);
		prop.load(fis);
		String browserName = prop.getProperty(browser);
		System.out.println(browserName);

		switch (browserName) {
		case "Firefox":
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			break;
		case "IE":
			System.setProperty("webdriver.ie.driver","C:\\Users\\abc\\Desktop\\Server\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
			break;
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", browserPath);
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			break;
		default:
			System.out.println("Drivers not initiated");

		}

		return driver;

	}


	public void closeAllDriver() {

		driver.close();
		driver.quit();
	}


@After
	public void screenshot(Scenario scenario) {
		if (scenario.isFailed()) {
			// Take a screenshot...

			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			// embed it in the report.
			scenario.attach(screenshot, "image/png", "userDir");
		}
	}


	public void inputData (WebElement locator, String txt) throws InterruptedException {
		locator.click();	
		locator.clear();
		locator.sendKeys(txt);
	}

	public void navigateURL (String url) throws IOException {
		String env = prop.getProperty(url);
		driver.get(env);
	}

	public void explicitWait(WebElement locator, int timeUnit ) {
		wait = new WebDriverWait  (driver, timeUnit);
	}


	public void elmtClick (WebElement locator, int clkwaitTime) {
		explicitWait(locator, clkwaitTime);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		locator.click();
	}

	public void elmtgetText (WebElement locator, int txtwaitTime, String verifytxt) {
		explicitWait(locator, txtwaitTime);
		wait.until(ExpectedConditions.textToBePresentInElement(locator, verifytxt));
		String msg = locator.getText();
		System.out.println(msg);
		Assert.assertEquals(msg, verifytxt);

	}

	public void isElmntDisplayed (WebElement locator) {

		try {
			if(locator.isDisplayed()) {
				System.out.println("Element is displayed");
			}
		} catch (Exception e) {
			System.out.println("Element is not displayed");
			//Assert.assertTrue(false);
		}


	}













}
