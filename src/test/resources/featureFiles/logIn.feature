Feature: Login Functionality
@login
Scenario Outline: Login outplay with incorrect credentials
Given user is on outplay "<browserdriver>" login page in "<environment>"
When user enters invalid "<username>","<password>" and clicks login
Then Error message is shown
Examples:
|browserdriver|environment|username|password|
|browserChr|stage|uName|invpwd|
#|browserChr|stage|uName|invpwd|

Scenario Outline: Login outplay with correct credentials
Given user is on outplay "<browserdriver>" login page in "<environment>"
When user enters valid "<username>","<password>" and clicks login
Then Dashboard is shown on the welcome page
Examples:
|browserdriver|environment|username|password|
|browserChr|stage|uName|pWord|


#Scenario: Click Prospects
#Given user is on welcome page
#When user clicks Prospect
#Then Add prospect is shown	


